package business;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import struts.action.Action;
import struts.form.ActionAnotation;
import struts.form.ActionForm;
import struts.form.ActionsAnotation;

public class SAction implements Action {

	@Override
	@ActionsAnotation(path="/useradd",
		beanName="", 
		actionClass="business.SAction",
		formClasss="business.SForm",
		mapurl={@ActionAnotation(name="failed",value="/view/UserFailder.jsp"),@ActionAnotation(name="success",value="/view/UserAddSuccess.jsp")})
	public String execute(HttpServletRequest request, ActionForm form,
			Map<String, String> actionForward) {
		
		String url = "failed";
		SForm sForm = (SForm)form;
		if(sForm.getName().equals("wdq")){
			url = "success";
		}
		
		return actionForward.get(url);
	}

}
